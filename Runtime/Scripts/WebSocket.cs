using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Wallid.Net.WebSockets
{
    public class WebSocket : MonoBehaviour
    {
        public UnityEvent OnOpened;
        public UnityEvent<string> OnMessage;
        public UnityEvent<string> OnError;
        public UnityEvent OnClosed;

        private ClientWebSocket clientWebSocket;
        private CancellationTokenSource cancellationTokenSource;

        public async void Connect(string url)
        {
            // Create a new WebSocket instance
            clientWebSocket = new ClientWebSocket();
            try
            {
                // Connect to the WebSocket server
                await clientWebSocket.ConnectAsync(new Uri(uriString: url), CancellationToken.None);

                //Debug.Log("Connection Openned");
                // Raise the connected event
                OnOpened?.Invoke();

                // Start a new thread to listen for incoming messages
                cancellationTokenSource = new CancellationTokenSource();
                _ = Task.Run(() => ReceiveLoop(cancellationTokenSource.Token));
            }
            catch (Exception ex)
            {
                //Debug.LogError($"WebSocket connection error: {ex.Message}");

                // Raise the error event
                OnError?.Invoke(ex.Message);
            }
        }

        public async void Disconnect(Uri uri = null)
        {
            await clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Connection closed by client", CancellationToken.None);
            OnClosed?.Invoke();
            cancellationTokenSource?.Cancel();
            cancellationTokenSource.Dispose();
        }

        private async void OnDestroy()
        {
            if (clientWebSocket != null && clientWebSocket.State == WebSocketState.Open)
            {
                // Close the WebSocket connection
                await clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Connection closed by client", CancellationToken.None);
                OnClosed?.Invoke();
            }

            // Cancel the receive loop
            cancellationTokenSource?.Cancel();
            cancellationTokenSource.Dispose();
        }

        private async Task ReceiveLoop(CancellationToken cancellationToken)
        {
            var receiveBuffer = new byte[1024]; // Buffer to store received data

            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    // Receive data from the WebSocket
                    var receiveResult = await clientWebSocket.ReceiveAsync(new ArraySegment<byte>(receiveBuffer), cancellationToken);

                    if (receiveResult.MessageType == WebSocketMessageType.Text)
                    {
                        // Process received text message
                        var message = System.Text.Encoding.UTF8.GetString(receiveBuffer, 0, receiveResult.Count);
                        //Debug.Log($"Received message: {message}");

                        // Raise the message received event
                        OnMessage?.Invoke(message);
                    }
                    else if (receiveResult.MessageType == WebSocketMessageType.Binary)
                    {
                        // Process received binary message
                        // TODO: Handle binary data
                    }
                    else if (receiveResult.MessageType == WebSocketMessageType.Close)
                    {
                        // WebSocket close message received, break the loop
                        break;
                    }
                }
                catch (WebSocketException ex)
                {
                    Debug.LogError($"WebSocket error: {ex.Message}");

                    // Raise the error event
                    OnError?.Invoke(ex.Message);
                    break;
                }
                catch (OperationCanceledException ex)
                {
                    // Operation canceled, break the loop
                    //Debug.LogError($"Error during WebSocket receive: {ex.Message}");
                    break;
                }
                catch (Exception ex)
                {
                    //Debug.LogError($"Error during WebSocket receive: {ex.Message}");

                    // Raise the error event
                    OnError?.Invoke(ex.Message);
                }
                finally
                {
                    /*await new WaitForUpdate();*/
                }
            }

            Debug.Log("Connection Closed");
            // Raise the disconnected event
            OnClosed?.Invoke();

            // Cancel the receive loop
            cancellationTokenSource?.Cancel();
            cancellationTokenSource.Dispose();
        }

        public async void Send(string message)
        {
            if (clientWebSocket.State == WebSocketState.Open)
            {
                try
                {
                    // Convert the message to a byte array
                    var sendBuffer = System.Text.Encoding.UTF8.GetBytes(message);

                    // Send the message through the WebSocket
                    await clientWebSocket.SendAsync(new ArraySegment<byte>(sendBuffer), WebSocketMessageType.Text, true, CancellationToken.None);
                }
                catch (WebSocketException ex)
                {
                    Debug.LogError($"WebSocket error: {ex.Message}");

                    // Raise the error event
                    OnError?.Invoke(ex.Message);
                }
                catch (Exception ex)
                {
                    Debug.LogError($"Error during WebSocket send: {ex.Message}");

                    // Raise the error event
                    OnError?.Invoke(ex.Message);
                }
            }
            else
            {
                Debug.LogWarning("WebSocket connection is not open");

                // Raise the error event
                OnError?.Invoke("WebSocket connection is not open");
            }
        }
    }
}